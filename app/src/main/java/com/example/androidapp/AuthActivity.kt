package com.example.androidapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class AuthActivity : AppCompatActivity() {
   // var user = User("Test", "test")
   var db: FirebaseFirestore? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        db = Firebase.firestore
    }

    fun readSetData(login: String, password: String): String{
        var role = ""
        var user: Task<QuerySnapshot> = db!!.collection("users")
                .whereEqualTo("name",login).whereEqualTo("password",password)
                .get()
                .addOnSuccessListener { documentReference ->
                    if (documentReference.documents.size > 0) {
                        role = documentReference.documents.get(0).get("role").toString()
                        val menuIntent = Intent (this, MenuActivity::class.java)
                        menuIntent.putExtra("role", role)
                        startActivity(menuIntent)
                    }
                    else {
                        val myToast = Toast.makeText(this, "Error login or password", Toast.LENGTH_SHORT)
                        myToast.show()
                    }
                }
        return role
    }


    fun openMenu(view: View) {
        var login = findViewById<EditText>(R.id.inputLogin)
        var password = findViewById<EditText>(R.id.inputPassword)

        var role = readSetData(login.text.toString(), password.text.toString())
    }


}