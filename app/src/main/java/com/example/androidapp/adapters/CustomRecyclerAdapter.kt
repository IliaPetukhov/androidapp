package com.example.androidapp.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidapp.DetailedActivity
import com.example.androidapp.R
import com.example.androidapp.model.People

class CustomRecyclerAdapter(private val names: List<People>?) :
    RecyclerView.Adapter<CustomRecyclerAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var largeTextView: TextView? = null
        var smallTextView: TextView? = null
        init {
            largeTextView = itemView.findViewById(R.id.textViewLarge)
            smallTextView = itemView.findViewById(R.id.textViewSmall)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.recyclerview_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var name: String? =  ""
        var nickname: String? = ""
        var job: String? = ""

        if(names?.get(position)?.name != null)
            name = names?.get(position)?.name!!
        if(names?.get(position)?.nickname != null)
            nickname = names?.get(position)?.nickname!!
        if(names?.get(position)?.job != null)
            job = names?.get(position)?.job!!

        holder.largeTextView?.text = nickname
        holder.smallTextView?.text = name

        // для перехода на новую форму по клику
        holder.itemView.setOnClickListener {
            val intent = Intent(holder.itemView.context, DetailedActivity::class.java)
            intent.putExtra("name", name)
            intent.putExtra("nickname", nickname)
            intent.putExtra("job", job)
            holder.itemView.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return names!!.size
    }
}