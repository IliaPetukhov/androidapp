package com.example.androidapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class User {
    @SerializedName("fio")
    @Expose
    var fio: String? = null

    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("mail")
    @Expose
    var mail: String? = null

}