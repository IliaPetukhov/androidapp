package com.example.androidapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserAuth {
    @SerializedName("login")
    @Expose
    var login: String? = null

    @SerializedName("password")
    @Expose
    var password: String? = null
}