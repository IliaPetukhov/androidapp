package com.example.androidapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidapp.adapters.CustomRecyclerAdapter
import com.example.androidapp.model.People
import com.example.androidapp.services.PeopleService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        val arguments = intent.extras
        var role = arguments!!.get("role").toString()
        if (role == "admin") {
            val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
            recyclerView.layoutManager = LinearLayoutManager(this)
            getAdvice()
        } else {
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(applicationContext, "Для вашей роли просмотр данного контента запрещен", duration)
            toast.show()
        }
    }

    fun getAdvice() {
        val callListPost = PeopleService.inctance?.JSONApi?.getAdvice()
        val callback = object : Callback<List<People>> {
            override fun onResponse(
                call: Call<List<People>>,
                response: Response<List<People>>
            ) {
                val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
                recyclerView.adapter = CustomRecyclerAdapter(response.body())
            }
            override fun onFailure(call: Call<List<People>>, t: Throwable) {
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(applicationContext, "Ошибка загрузки", duration)
                toast.show()
            }
        }
        callListPost?.enqueue(callback)
    }
}