package com.vsu.myapplication

import com.example.androidapp.model.People
import com.example.androidapp.model.User
import com.example.androidapp.model.UserAuth
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface JSONPlaceholderApi {
    @GET("/data")
    fun getAdvice(): Call<List<People>>

    @POST("/student/auth")
    fun auth(@Body auth : UserAuth): Call<User>
}