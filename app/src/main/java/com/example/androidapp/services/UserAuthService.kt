package com.example.androidapp.services

import com.google.gson.GsonBuilder
import com.vsu.myapplication.JSONPlaceholderApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class UserAuthService {
    val gsonBuilder: GsonBuilder = GsonBuilder();

    private val mRetforit: Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(
                    GsonConverterFactory.create(gsonBuilder.setLenient().create())
            ).build()
    val JSONApi: JSONPlaceholderApi
        get() = mRetforit.create(JSONPlaceholderApi::class.java)

    companion

    object {
        private var mInstance: UserAuthService? = null
        private const val BASE_URL = "https://kekes-lol.herokuapp.com/"
        val inctance: UserAuthService?
            get() {
                if (mInstance == null) {
                    mInstance = UserAuthService()
                }
                return mInstance;
            }
    }

}