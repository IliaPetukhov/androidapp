package com.example.androidapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class DetailedActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailed)
        val arguments = intent.extras

        val nickname : TextView = findViewById(R.id.nickname)
        val name: TextView = findViewById(R.id.name)
        val job: TextView = findViewById(R.id.job)

        nickname.text = arguments!!.get("nickname").toString()
        name.text = arguments!!.get("name").toString()
        job.text = arguments!!.get("job").toString()
    }
}